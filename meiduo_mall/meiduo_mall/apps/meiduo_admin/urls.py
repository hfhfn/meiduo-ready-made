from django.conf.urls import url
from rest_framework.routers import DefaultRouter
from rest_framework_jwt.views import obtain_jwt_token

from meiduo_admin.views import images
from meiduo_admin.views import option
from meiduo_admin.views import skus
from meiduo_admin.views import specs
from meiduo_admin.views import statistical
from meiduo_admin.views import users

urlpatterns = [
    url(r'^authorizations/$', obtain_jwt_token),


    url(r'^statistical/total_count/$', statistical.UserTotalCountView.as_view()),
    url(r'^statistical/day_increment/$', statistical.UserDayCountView.as_view()),
    url(r'^statistical/day_active/$', statistical.UserActiveCountView.as_view()),
    url(r'^statistical/day_orders/$', statistical.UserOrderCountView.as_view()),
    url(r'^statistical/month_increment/$', statistical.UserMonthCountView.as_view()),
    url(r'^statistical/goods_day_views/$', statistical.GoodsDayView.as_view()),


    url(r'^users/$', users.UserView.as_view()),


    url(r'^goods/simple', specs.SpecsView.as_view({'get': 'simple'})),
    url(r'^skus/simple', images.ImageView.as_view({'get': 'simple'})),
    url(r'^goods/(?P<pk>\d+)/specs', skus.SKUView.as_view({'get': 'specs'})),
    url(r'^goods/specs/simple', option.OptionView.as_view({'get': 'simple'})),

]

router = DefaultRouter()
router.register('goods/specs', specs.SpecsView, base_name='specs')
urlpatterns += router.urls


router = DefaultRouter()
router.register('skus/images', images.ImageView, base_name='images')
urlpatterns += router.urls


router = DefaultRouter()
router.register('skus', skus.SKUView, base_name='skus')
urlpatterns += router.urls


router = DefaultRouter()
router.register('specs/options', option.OptionView, base_name='options')
urlpatterns += router.urls