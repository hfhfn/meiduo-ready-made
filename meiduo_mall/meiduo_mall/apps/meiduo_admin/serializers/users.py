import re
from rest_framework import serializers
from users.models import User

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model=User
        fields=('id','username','mobile','email', 'password')

        extra_kwargs = {
            'password': {
                'write_only': True,
                'max_length': 20,
                'min_length': 8
            },
            'username': {
                'max_length': 20,
                'min_length': 5
            }
        }

    def validate_mobile(self, value):

        if not re.match('1[3-9][0-9]{9}', value):

            raise serializers.ValidationError('手机格式不匹配')

        return value

    def create(self, validated_data):

        # 第一种加密密码的方法
        user = super().create(validated_data)
        user.set_password(validated_data['password'])
        user.save()

        #第二种加密密码的方法
        # user = User.objects.create_user(**validated_data)

        return user