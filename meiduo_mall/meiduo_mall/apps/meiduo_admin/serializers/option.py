from rest_framework import serializers

from goods.models import SpecificationOption, SPUSpecification


class SpecificationOptionSerializer(serializers.ModelSerializer):

    spec = serializers.StringRelatedField(read_only=True)
    spec_id = serializers.IntegerField()

    class Meta:

        model = SpecificationOption
        fields = ('id', 'value', 'spec', 'spec_id')