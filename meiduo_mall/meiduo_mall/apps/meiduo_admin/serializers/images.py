from django.conf import settings
from fdfs_client.client import Fdfs_client
from rest_framework import serializers
from goods.models import SKUImage, SKU
from celery_tasks.static_html.tasks import get_detail_html


class SKUImageSerializer(serializers.ModelSerializer):

    sku = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = SKUImage
        fields = ('id', 'sku', 'image')

    def create(self, validated_data):
        client = Fdfs_client(settings.FASTDFS_PATH)
        request = self.context['request']
        image_file = request.data.get('image')

        res = client.upload_by_buffer(image_file.read())
        if res['Status'] != 'Upload successed.':
            raise serializers.ValidationError('图片上传失败')

        image_url = res['Remote file_id']
        img = SKUImage.objects.create(image=image_url, sku_id=request.data['sku'])

        sku = SKU.objects.get(id=request.data['sku'])
        if not sku.default_image:
            sku.default_image = image_url
            sku.save()
        get_detail_html.delay(request.data['sku'])
        return img

    def update(self, instance, validated_data):
        client = Fdfs_client(settings.FASTDFS_PATH)
        request = self.context['request']
        image_file = request.data.get('image')

        res = client.upload_by_buffer(image_file.read())
        if res['Status'] != 'Upload successed.':
            raise serializers.ValidationError('图片上传失败')

        image_url = res['Remote file_id']
        instance.image = image_url
        instance.sku_id = request.data['sku']
        instance.save()

        sku = SKU.objects.get(id=request.data['sku'])
        if not sku.default_image:
            sku.default_image = image_url
            sku.save()
        get_detail_html.delay(request.data['sku'])

        return instance

class SKUSerializer(serializers.ModelSerializer):

    class Meta:
        model = SKU
        fields = ('id', 'name')