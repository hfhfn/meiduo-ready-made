from rest_framework.decorators import action
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from goods.models import SPUSpecification, SKU, GoodsCategory, SpecificationOption
from meiduo_admin.serializers.skus import SKUSerializer, GoodsCategorySerializer, \
    SPUSpecificationSerializer
from meiduo_admin.utils import PageNum


class SKUView(ModelViewSet):

    queryset = SKU.objects.all()
    serializer_class = SKUSerializer
    pagination_class = PageNum
    permission_classes = [IsAdminUser]


    # 重写get_queryset方法，根据前端是否传递keyword值返回不同查询结果
    def get_queryset(self):
        # 获取前端传递的keyword值
        keyword = self.request.query_params.get('keyword')
        # 如果keyword是空字符，则说明要获取所有用户数据
        if keyword is '' or keyword is None:
            return SKU.objects.all()
        else:
            return SKU.objects.filter(name__contains= keyword)

    @action(methods=['get'], detail=False)
    def categories(self, request):

        data = GoodsCategory.objects.filter(subs=None)

        ser = GoodsCategorySerializer(data, many=True)

        return Response(ser.data)

    def specs(self, request, pk):

        data = SPUSpecification.objects.filter(spu_id=pk)

        ser = SPUSpecificationSerializer(data, many=True)

        return Response(ser.data)

