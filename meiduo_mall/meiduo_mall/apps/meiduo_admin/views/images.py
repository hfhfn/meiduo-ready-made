from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from goods.models import SKUImage, SKU
from meiduo_admin.serializers.images import SKUImageSerializer, SKUSerializer
from meiduo_admin.utils import PageNum


class ImageView(ModelViewSet):

    queryset = SKUImage.objects.all()
    serializer_class = SKUImageSerializer
    pagination_class = PageNum
    permission_classes = [IsAdminUser]

    def simple(self, request):

        skus = SKU.objects.all()
        ser = SKUSerializer(skus, many=True)
        return Response(ser.data)