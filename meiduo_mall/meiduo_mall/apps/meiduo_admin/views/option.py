from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from goods.models import SpecificationOption
from meiduo_admin.serializers.option import SpecificationOptionSerializer
from meiduo_admin.utils import PageNum


class OptionView(ModelViewSet):

    queryset = SpecificationOption.objects.all()

    serializer_class = SpecificationOptionSerializer

    pagination_class = PageNum

    permission_classes = [IsAdminUser]

    def simple(self, request):

        options = SpecificationOption.objects.all()
        ser = SpecificationOptionSerializer(options, many=True)

        return Response(ser.data)
