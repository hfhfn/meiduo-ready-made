from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from goods.models import SPUSpecification, SPU
from meiduo_admin.serializers.specs import SPUSpecificationSerializer, SPUSerializer
from meiduo_admin.utils import PageNum


class SpecsView(ModelViewSet):

    queryset = SPUSpecification.objects.all()

    serializer_class = SPUSpecificationSerializer

    pagination_class = PageNum

    permission_classes = [IsAdminUser]

    def simple(self, request):

        spus = SPU.objects.all()
        ser = SPUSerializer(spus, many=True)

        return Response(ser.data)
